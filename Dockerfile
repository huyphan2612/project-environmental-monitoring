#Deriving the latest base image
FROM python:3.10

#Labels as key value pair
LABEL Maintainer="huyphan"

# Set timezone in docker container
ENV TZ=Asia/Bangkok

# Sets the working directory in the container
WORKDIR /usr/app/src

# Copies the dependency files to the working directory
COPY requirements.txt ./

# Install dependencies
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# Copy station data into the working directory
COPY data/ ./data/

# Copy environment file to the working directory
COPY .env ./

# Copy python files to the working directory
COPY *.py ./

# Set entrypoint
ENTRYPOINT ["python3"]