# VIETNAM ENVIRONMENTAL MONITORING API
This repository contains data pipelines for extracting air quality data from [CEM](https://cem.gov.vn) and [Air Quality Open Data Platform](https://aqicn.org) in Vietnam. You can use this repository to get air quality data and load data into your data warehouse for environmental analysis. This repository is open source and please feel free to contribute to this project.

### I. How to use our data pipelines
1. You need to create ```.env file``` in the root directory with your data warehouse credentials.
2. You can change your destination table's name as your wish.
4. You can run the data pipeline now. It should work!
```
# To extract data from CEM - run this command and replace docker_name with your actual docker name
$ docker run -t docker_name:lastest vietnam_airquality_cem.py
# To extract air quality data from AQICN - run this command and replace docker_name with your actual docker name
$ docker run -t docker_name:lastest vietnam_airquality_aqicn.py
```

### II. How to get station id from CEM
You can follow these steps to get station_id from [CEM website](https://cem.gov.vn) 
1. Access [CEM website](https://cem.gov.vn) 
2. Right-click and select ```Inspect element``` (I'm using Opera browser)
3. Use your mouse and point to station list on the web (station section) to inspect the html code
4. You will see attribute ```data-id``` in div tag - the value of ```data-id``` is our station_id

### III. Air Quality Open Data Platform
#### 1. Retrieve API token from [Air Quality Open Data Platform](https://aqicn.org)
1. Access [Air Quality Token Page](https://aqicn.org/data-platform/token/)
2. Enter your email address, your fullname and agree with the Terms of Service
3. Click *Submit*
4. Now, the token should be sent to your email address
5. Make sure that you need to replace your new token into ```WAQI_TOKEN``` environment variable

#### 2. API Usage
- All the APIs are provided for free.
- A valid key must be used for accessing the API.
- All the API are subjected to quota.

The default quota is 1,000 (one thousand) requests per second.
For more information, you can use the [online API documentation](https://aqicn.org/json-api/doc/).