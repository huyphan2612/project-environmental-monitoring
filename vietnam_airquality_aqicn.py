from sqlalchemy import create_engine
from os import environ, path
from dotenv import load_dotenv
from datetime import datetime
from geoalchemy2 import Geometry

import pandas as pd
import geopandas as gpd
import requests
import json

# load .env - remember replace sample .env with your .env
# If you're using the code inside a .py file: Use os.path.abspath(__file__)
# If you're using the code on a script directly or in Jupyter Notebooks: Put the file inside double-quotes. os.path.abspath("__file__")
basedir = path.abspath(path.dirname("__file__"))
load_dotenv(path.join(basedir, '.env'))

DWH_USERNAME = environ.get('DWH_USERNAME')
DWH_PASSWORD = environ.get('DWH_PASSWORD')
DWH_SERVER = environ.get('DWH_SERVER')
DWH_PORT = environ.get('DWH_PORT')
SPATIAL_DB_NAME = environ.get('SPATIAL_DB_NAME')

WAQI_TOKEN = environ.get('WAQI_TOKEN')

# Creating SQLAlchemy's engine to use
engine = create_engine('postgresql+psycopg2://{0}:{1}@{2}:{3}/{4}'.format(DWH_USERNAME, DWH_PASSWORD, DWH_SERVER, DWH_PORT, SPATIAL_DB_NAME))


class Data_Process():

    def __init__(self):
        self.table_name = 'vietnam_airquality_aqicn'
        self.mapbound_endpoint = 'https://api.waqi.info/v2/map/bounds'
        self.mapbound_coordinates = '23.583612,102.050278,8,109.666945'  # map bounds in the form lat1,lng1,lat2,lng2
        self.feed_endpoint = 'https://api.waqi.info/feed/'

    def get_station_list(self):
        web_response = requests.get(self.mapbound_endpoint, params={"token": WAQI_TOKEN, 'latlng': self.mapbound_coordinates}, verify=True)
        data = json.loads(web_response.content)
        if data['status'] == 'ok':
            df = pd.json_normalize(data['data'], sep='_')
            df = df[(df['station_name'].str.contains('Vietnam')) | df['station_name'].str.contains('VN')]
            df['station_time'] = pd.to_datetime(df['station_time']).dt.tz_convert('Asia/Bangkok')
            df = df[(df['station_time'].dt.date == datetime.now().date()) & (df['station_time'].dt.hour == datetime.now().hour)]  # filter only today and current hour data
            df['uid'] = df['uid'].astype(pd.StringDtype())  # convert object to string data type
            station_list = [x.replace('-', 'A') for x in df.uid.to_list()]  # replace minus sign in number to A letter
            return station_list

    def get_aqicn_data(self, station_id):
        web_response = requests.get("@".join([self.feed_endpoint, str(station_id)]), params={"token": WAQI_TOKEN}, verify=True)
        data = json.loads(web_response.content)
        if data['status'] == 'ok':
            df = pd.json_normalize(data['data'], sep='_')
            df = df.loc[:, ~df.columns.str.startswith('forecast')]  # drop columns starting with forecast
            df['attributions'] = df['attributions'].str[1]  # select the second element in list
            df = pd.concat([df.drop(['attributions'], axis=1), df['attributions'].apply(pd.Series)], axis=1)  # explode dict
            df['time_iso'] = pd.to_datetime(df['time_iso']).dt.tz_convert('Asia/Bangkok')  # convert object to datetime & convert to Asia/Bangkok timezone
            df['aqi'] = df['aqi'].replace('-', None)
            df = df.replace(r'^\s*$', None, regex=True)  # replace empty string to None value
            gdf = (
                gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.city_geo.str[1], df.city_geo.str[0]))
                .drop(columns=['city_geo'], inplace=False)
            )
            return gdf

    def process_aqicn_data(self):
        station_list = self.get_station_list()
        response_list = [self.get_aqicn_data(station_id) for station_id in station_list]
        response_list = list(filter(lambda x: x is not None, response_list))  # remove None item in list
        gdf = pd.concat(response_list, ignore_index=True)
        gdf.drop(columns=['debug_sync', 'time_s', 'time_tz', 'time_v', 'iaqi_aqi_v'], inplace=True, errors='ignore')
        gdf.rename(columns={
            'iaqi_co_v': 'iaqi_co',
            'iaqi_dew_v': 'iaqi_dew',
            'iaqi_h_v': 'iaqi_humidity',
            'iaqi_no2_v': 'iaqi_no2',
            'iaqi_o3_v': 'iaqi_o3',
            'iaqi_p_v': 'iaqi_pressure',
            'iaqi_pm10_v': 'iaqi_pm10',
            'iaqi_pm25_v': 'iaqi_pm25',
            'iaqi_r_v': 'iaqi_rain',
            'iaqi_so2_v': 'iaqi_so2',
            'iaqi_t_v': 'iaqi_temp',
            'iaqi_w_v': 'iaqi_wind',
            'iaqi_wg_v': 'iaqi_wind_gust',
        }, inplace=True)
        gdf = gdf[(gdf['time_iso'].dt.date == datetime.now().date()) & (gdf['time_iso'].dt.hour == datetime.now().hour)]  # filter only today and current hour data
        gdf = gdf.set_crs(epsg=4326)  # set epsg
        return gdf

    def save2postgres(self, df):
        df.to_postgis(
            con=engine,
            schema='public',
            name=self.table_name,
            if_exists='append',
            index=False,
            dtype={'geometry': Geometry('POINT', srid=4326)}
        )

    def execute(self):
        df = self.process_aqicn_data()
        self.save2postgres(df)


if __name__ == '__main__':
    Data_Process().execute()
