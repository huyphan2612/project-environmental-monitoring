from sqlalchemy import create_engine
from os import environ, path
from dotenv import load_dotenv
from datetime import datetime
from geoalchemy2 import Geometry

import pandas as pd
import geopandas as gpd
import requests
import json

# load .env - remember replace sample .env with your .env
# If you're using the code inside a .py file: Use os.path.abspath(__file__)
# If you're using the code on a script directly or in Jupyter Notebooks: Put the file inside double-quotes. os.path.abspath("__file__")
basedir = path.abspath(path.dirname("__file__"))
load_dotenv(path.join(basedir, '.env'))

DWH_USERNAME = environ.get('DWH_USERNAME')
DWH_PASSWORD = environ.get('DWH_PASSWORD')
DWH_SERVER = environ.get('DWH_SERVER')
DWH_PORT = environ.get('DWH_PORT')
SPATIAL_DB_NAME = environ.get('SPATIAL_DB_NAME')

# Creating SQLAlchemy's engine to use
engine = create_engine('postgresql+psycopg2://{0}:{1}@{2}:{3}/{4}'.format(DWH_USERNAME, DWH_PASSWORD, DWH_SERVER, DWH_PORT, SPATIAL_DB_NAME))


class Data_Process():

    def __init__(self):
        self.table_name = 'vietnam_airquality_cem'
        self.endpoint = 'http://envisoft.gov.vn/eos/services/call/json/qi_detail'
        self.station_uri = '{0}/data/stations.csv'.format(basedir)
        self.station_mapping_uri = '{0}/data/station_mapping.json'.format(basedir)

    def get_station_mapping(self):
        df = pd.read_json(self.station_mapping_uri, orient='records', dtype=False)[['station_id', 'lat', 'lon', 'uid']]
        df.rename(columns={'lat': 'latitude', 'lon': 'longitude', 'uid': 'idx'}, inplace=True)
        return df

    def get_station_list(self):
        return pd.read_csv(self.station_uri, header=0).station_id.to_list()

    def get_cem_data(self, station_id):
        web_response = requests.post(self.endpoint, params={"station_id": station_id}, verify=True)
        data = json.loads(web_response.content)
        if data['success'] is True:
            data_dict = {
                'qi_time': data.get('qi_time_2'),
                'co': data.get('res').get('CO')['current'] if data.get('res').get('CO') is not None else None,
                'no2': data.get('res').get('NO2')['current'] if data.get('res').get('NO2') is not None else None,
                'o3': data.get('res').get('O3')['current'] if data.get('res').get('O3') is not None else None,
                'pm10': data.get('res').get('PM-10')['current'] if data.get('res').get('PM-10') is not None else None,
                'pm2_5': data.get('res').get('PM-2-5')['current'] if data.get('res').get('PM-2-5') is not None else None,
                'so2': data.get('res').get('SO2')['current'] if data.get('res').get('SO2') is not None else None,
                'qi_description': data.get('qi_detail_info').get('description'),
                'qi_idx_text': data.get('qi_detail_info').get('text'),
                'qi_idx_no': data.get('qi_detail_info').get('to'),
                'qi_value': data.get('qi_value'),
                'station_name': data.get('station_name'),
                'station_id': station_id,
            }
            return data_dict

    def process_cem_data(self):
        station_list = self.get_station_list()
        response_list = [self.get_cem_data(station_id) for station_id in station_list]
        df = pd.DataFrame(list(filter(lambda x: x is not None, response_list)))  # remove None item in list
        df['qi_time'] = pd.to_datetime(df['qi_time'], format='%d/%m/%Y %H:%M').dt.tz_localize('Asia/Bangkok')  # convert string to datetime
        df['qi_value'] = df['qi_value'].replace('-', None)
        df = df[(df['qi_time'].dt.date == datetime.now().date()) & (df['qi_time'].dt.hour == datetime.now().hour)]  # filter only today and current hour data
        df = df.set_index('station_id').join(self.get_station_mapping().set_index('station_id')).reset_index()
        gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.longitude, df.latitude))
        gdf = gdf.set_crs('epsg:4326')
        return gdf

    def save2postgres(self, df):
        df.to_postgis(
            con=engine,
            schema='public',
            name=self.table_name,
            if_exists='append',
            index=False,
            dtype={'geometry': Geometry('POINT', srid=4326)}
        )

    def execute(self):
        df = self.process_cem_data()
        self.save2postgres(df)


if __name__ == '__main__':
    Data_Process().execute()
