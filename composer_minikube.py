#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
from __future__ import annotations
from airflow import DAG
from airflow.operators.docker_operator import DockerOperator

import datetime
import pendulum

default_args = {
    'owner': 'huy.phan',
    'depends_on_past': False,
    'email': ['huyphan2612@gmail.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 3,
    'retry_delay': datetime.timedelta(minutes=5),
}

with DAG(
    dag_id='project-environmental-monitoring',
    description='This data pipeline will extract data from open weather map',
    schedule_interval='30 * * * *',
    start_date=pendulum.datetime(2022, 12, 2, 10, 30, 0, tz="Asia/Saigon"),
    dagrun_timeout=datetime.timedelta(minutes=5),
    tags=['environmental', 'monitoring', 'api', 'cem', 'air', 'quality'],
    catchup=False,
    concurrency=1,
    default_args=default_args
) as dag:

    extract_vietnam_airquality_cem = DockerOperator(
        task_id='extract_vietnam_airquality_cem',
        image='rregistry.gitlab.com/huyphan2612/project-environmental-monitoring:0.0.1',
        command='vietnam_airquality_cem.py',
        auto_remove=True,
        force_pull=False,
        docker_conn_id='gitlab_container_registry_conn_id',
        docker_url='tcp://docker-socket-proxy-svc:2376',
        api_version='auto',
        dag=dag
    )

    extract_vietnam_airquality_aqicn = DockerOperator(
        task_id='extract_vietnam_airquality_aqicn',
        image='registry.gitlab.com/huyphan2612/project-environmental-monitoring:0.0.1',
        command='vietnam_airquality_aqicn.py',
        auto_remove=True,
        force_pull=False,
        docker_conn_id='gitlab_container_registry_conn_id',
        docker_url='tcp://docker-socket-proxy-svc:2376',
        api_version='auto',
        dag=dag

extract_vietnam_airquality_cem
extract_vietnam_airquality_aqicn


if __name__ == "__main__":
    dag.cli()